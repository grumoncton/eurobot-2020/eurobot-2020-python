import time
import curses
from enum import IntEnum
from multiprocessing import Process

from .ports import list_ports, map_ports


class ColorCodes(IntEnum):
    OK = 1
    WARN = 2
    ERR = 3


def worker(device, callback):
    while True:
        callback(device, update_device(device))
        time.sleep(1)


def update_device(device):
    if device.is_in_bootloader():
        return (
            (device.port, curses.A_NORMAL),
            ('-', curses.A_NORMAL),
            ('-', curses.A_NORMAL),
            ('In Bootloader', curses.color_pair(ColorCodes.WARN.value)),
        )

    device.get_info()
    return (
        (device.port, curses.A_NORMAL),
        (device.name, curses.A_NORMAL),
        (str(device.firmware), curses.A_NORMAL),
        ('Ready', curses.color_pair(ColorCodes.OK.value)),
    )


def main(stdscr):
    def render_screen(rows):
        stdscr.clear()
        widths = [0] * len(rows[0])

        for i, row in enumerate(rows):
            for j, column in enumerate(row):
                text, _ = column
                widths[j] = max([len(text), widths[j]])

        for i, row in enumerate(rows):
            for j, column in enumerate(row):
                text, attr = column
                stdscr.addstr(
                    i,
                    sum(widths[:j]) + j if j > 0 else 0,
                    f'{text: <{widths[j] + 1}}',
                    attr,
                )

    def update(device, row):
        i = ports.index(device.port)
        body[i] = row
        render_screen([columns] + body)
        stdscr.refresh()

    stdscr.clear()
    curses.curs_set(0)
    curses.init_pair(ColorCodes.OK.value, curses.COLOR_BLACK, curses.COLOR_GREEN)
    curses.init_pair(ColorCodes.WARN.value, curses.COLOR_RED, curses.COLOR_BLACK)
    curses.init_pair(ColorCodes.ERR.value, curses.COLOR_BLACK, curses.COLOR_RED)

    columns = [
        ('Port', curses.A_BOLD),
        ('Name', curses.A_BOLD),
        ('Firmware', curses.A_BOLD),
        ('State', curses.A_BOLD),
    ]

    render_screen([columns])
    stdscr.refresh()

    ports = list_ports()
    devices = map_ports(ports)
    body = [None] * len(ports)

    threads = []

    for device in devices:
        threads.append(Process(target=worker, args=(device, update)))

    for thread in threads:
        thread.start()
        thread.join()

    while True:
        time.sleep(1)


def init():
    curses.wrapper(main)
