from queue import Empty as QueueEmpty
import tkinter as tk
import numpy as np

from lib import Coord, ResizingCanvas
from drive.differential import drive


CIRCLE_SIZE = 5


def cart2pol(x, y):
    rho = np.sqrt(x**2 + y**2)
    phi = np.arctan2(y, x)
    return (rho, phi)


def pol2cart(rho, phi):
    x = rho * np.cos(phi)
    y = rho * np.sin(phi)
    return (x, y)


class Visualizer():
    def __init__(self, queue):
        from . import lidar

        self.queue = queue

        self._root = tk.Tk()

        frame = tk.Frame(self._root, bg='black')
        frame.pack(fill='both', expand=True)

        self._size = 2000
        self._center = Coord(
            x=self._size / 2,
            y=self._size / 2,
        )
        self._canvas = ResizingCanvas(
            parent=frame,
            width=self._size,
            height=self._size,
            bg='black',
            borderwidth=0,
            highlightthickness=0,
        )
        self._canvas.pack(side="top", fill="both", expand=True)

        self._canvas.bind('<B1-Motion>', self._move_opponent)
        self._canvas.bind('<ButtonRelease-1>', self._remove_opponent)

        self._canvas.create_line(
            self._center.x - 10,
            self._center.y,
            self._center.x + 10,
            self._center.y,
            fill='white',
        )

        self._canvas.create_line(
            self._center.x,
            self._center.y - 10,
            self._center.x,
            self._center.y + 10,
            fill='white',
        )

        # Draw robot
        self._canvas.create_rectangle(
            self._center.x - lidar._ROBOT_WIDTH / 2,
            self._center.y - lidar._ROBOT_LENGTH / 2,
            self._center.x + lidar._ROBOT_WIDTH / 2,
            self._center.y + lidar._ROBOT_LENGTH / 2,
            fill='',
            outline='white',
        )

        self._canvas.create_rectangle(
            self._center.x - lidar._ROBOT_WIDTH / 2,
            self._center.y,
            self._center.x + lidar._ROBOT_WIDTH / 2,
            self._center.y -
            (lidar._ROBOT_LENGTH / 2 + drive.brake_distance()),
            fill='',
            outline='red',
        )

        self.stop_shape = lidar.get_stop_shape()

        # Draw stop shape
        self._canvas.create_oval(
            self._center.x + (self.stop_shape.x - self.stop_shape.rx),
            self._center.y - (self.stop_shape.y - self.stop_shape.ry),
            self._center.x + (self.stop_shape.x + self.stop_shape.rx),
            self._center.y - (self.stop_shape.y + self.stop_shape.ry),
            fill='',
            outline='red',
        )

        self.slow_shape = lidar.get_slow_shape(self.stop_shape)

        # Draw slow shape
        self._canvas.create_oval(
            self._center.x + (self.slow_shape.x - self.slow_shape.rx),
            self._center.y - (self.slow_shape.y - self.slow_shape.ry),
            self._center.x + (self.slow_shape.x + self.slow_shape.rx),
            self._center.y - (self.slow_shape.y + self.slow_shape.ry),
            fill='',
            outline='yellow',
        )

        self._root.after(0, self.redraw)

        self._root.mainloop()

    def redraw(self):
        try:
            points = self.queue.get(0)
            self._canvas.delete('point')

            for _, phi, rho in points:
                x, y = (
                    np.interp(x, [-1000, 1000], [0, self._size])
                    for x in pol2cart(
                        rho=rho,
                        phi=(phi - 90) / 180 * np.pi,
                    )
                )

                self._canvas.create_oval(
                    x - CIRCLE_SIZE,
                    y - CIRCLE_SIZE,
                    x + CIRCLE_SIZE,
                    y + CIRCLE_SIZE,
                    fill='red',
                    tags='point',
                )

        except QueueEmpty:
            pass

        finally:
            self._root.after(1, self.redraw)

    def _remove_opponent(self, *args, **kwargs):
        # pylint: disable=unused-argument

        self._canvas.delete('opponent')

    def _move_opponent(self, event):
        from . import lidar

        self._remove_opponent()

        x, y = self._canvas.event_coords(event)

        colour = 'red' if self.stop_shape.is_point_inside(
            x=x - self._center.x,
            y=self._center.y - y,
        ) and y < self._center.y else (
            'yellow' if self.slow_shape.is_point_inside(
                x=x - self._center.x,
                y=self._center.y - y,
            ) else 'white'
        )

        self._canvas.create_oval(
            x - lidar.OPPONENT_RADIUS,
            y - lidar.OPPONENT_RADIUS,
            x + lidar.OPPONENT_RADIUS,
            y + lidar.OPPONENT_RADIUS,
            fill='',
            outline=colour,
            tags='opponent',
        )

        self._canvas.create_oval(
            x - lidar.BEACON_RADIUS,
            y - lidar.BEACON_RADIUS,
            x + lidar.BEACON_RADIUS,
            y + lidar.BEACON_RADIUS,
            fill=colour,
            tags='opponent',
        )
