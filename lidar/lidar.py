import math
import time
import threading
from queue import Queue
from enum import IntEnum
import rplidar

from lib import Coord, ContextManager, dotenv
from lib.functional import pairwise
from drive.differential import drive
from pathfinder import pathfinder, Circle
from positioning import pos


LIDAR_OFFSET = float(dotenv.get_value('LIDAR_OFFSET'))
MOCK_LIDAR = bool(dotenv.get_value('MOCK_LIDAR'))


class Ellipse:
    def __init__(self, x, y, rx, ry):
        self.x = x
        self.y = y
        self.rx = rx
        self.ry = ry

    def is_point_inside(self, x, y):
        return ((x - self.x)**2) / (self.rx**2) + \
            ((y - self.y)**2) / (self.ry**2) < 1


class LidarManager:
    # pylint: disable=too-many-instance-attributes

    # TODO: Something with this
    OPPONENT_RADIUS = 200

    # TODO: Find a better place for this
    _ROBOT_LENGTH = 180
    _ROBOT_WIDTH = 246.35

    # Distance we want to come to hitting opponents
    _CLEARANCE = 100

    # Radius of beacon thingy on top of robots
    BEACON_RADIUS = 50

    # Distance to offset the zone that slows the robot from the zone that stops
    # the robot
    _SLOW_OFFSET = 200

    # Detection distance when rotating
    _ROTATION_STOP_DISTANCE = 80
    _ROTATION_SLOW_DISTANCE = 160

    class MeasureImportance(IntEnum):
        SLOW = 0
        STOP = 1

    def __init__(self, port):
        self.port = port
        self.lidar = None
        self.stop_event = threading.Event()
        self.slow_event = threading.Event()
        self.ready = threading.Event()

        self._stop_shape = None
        self._slow_shape = None

    def init(self):
        if MOCK_LIDAR:
            from unittest.mock import MagicMock
            self.lidar = MagicMock()
        else:
            self.lidar = rplidar.RPLidar(port=self.port)

        return ContextManager(on_exit=self.stop)

    def _annotate_measures(self, measures):
        """
        For each important measure (close enough to maybe be a problem), check
        if it should make the robot stop of if it should just make it slow
        down.

        Generator that yields all important measures and their importance as a
        tuple.
        """

        for measure in measures:

            _, angle, distance = measure

            x = distance * math.cos(angle + LIDAR_OFFSET)
            y = distance * math.sin(angle + LIDAR_OFFSET)

            if drive.current_movement_type == \
                    drive.MovementType.IDLE:
                continue

            elif drive.current_movement_type == \
                    drive.MovementType.STRAIGHT:

                if y > 0 and self._stop_shape.is_point_inside(x=x, y=y):
                    yield Coord(x, y), self.MeasureImportance.STOP

                elif self._slow_shape.is_point_inside(x=x, y=y):
                    yield Coord(x, y), self.MeasureImportance.SLOW

            elif drive.current_movement_type == \
                    drive.MovementType.TURNING:

                if distance < self._ROTATION_STOP_DISTANCE:
                    yield Coord(x, y), self.MeasureImportance.STOP

                elif distance < self._ROTATION_SLOW_DISTANCE:
                    yield Coord(x, y), self.MeasureImportance.SLOW

            else:
                raise NotImplementedError()

    def _measures_to_obstacles(self, measures):
        clumps = [[measures[0][0]]]

        CLUMPING_DISTANCE = self.BEACON_RADIUS * 2

        # Measures have contiguous angle so in theory they should only make up
        # the same obstacle if they come one after the other
        for (prev, _), (measure, _) in pairwise(measures):

            # Add new clump if measure too far from last
            if math.hypot(prev.x - measure.x, prev.y - measure.y) < \
                    CLUMPING_DISTANCE:
                clumps.append([])

            clumps[-1].append(measure)

        for clump in clumps:
            x = 0
            y = 0

            for coord in clump:
                x += coord.x
                y += coord.y

            yield Circle(
                x=x / len(clump),
                y=y / len(clump),
                r=self.OPPONENT_RADIUS,
            )

    @staticmethod
    def _without_false_positives(scan):
        """
        Remove false positives from scan. Remove any points that come from
        table obstacles, game elements, beacons, the acrylic tube surrounding
        the lidar, or anything off of the table.

        This year, there aren't really any elements on the table, so just check
        that the measures aren't off the table.
        """

        ACRYLIC_DISTANCE = 2
        ERROR = 50

        min_x = ERROR - pos.x
        max_x = min_x + pathfinder.TABLE_HEIGHT
        min_y = ERROR - pos.y
        max_y = min_y + pathfinder.TABLE_WIDTH

        for measure in scan:
            _, angle, distance = measure

            # Skip if the lidar picked up the acrylic
            if distance < ACRYLIC_DISTANCE:
                continue

            absolute_angle = angle + LIDAR_OFFSET + pos.a

            x = distance * math.cos(absolute_angle)
            y = distance * math.sin(absolute_angle)

            if min_x < x < max_x and min_y < y < max_y:
                yield measure

    def mainloop(self):
        time.sleep(5)

        try:
            for scan in self.lidar.iter_scans():
                self.ready.set()

                filtered_scan = self._without_false_positives(scan)

                # Update shapes with current deceleration distance every scan
                # This is a nice compromise between updating it for each
                # measure and never updating it
                self._update_stop_shape()

                # Don't update slow shape if the slow event set
                # The size of the shape is based on the speed of the robot, so
                # if we make it slow down, the obstacle that made it slow don't
                # wont be inside the shape anymore and it will speed back up
                # and get in a loop
                if not self.slow_event.is_set():
                    self._update_slow_shape()

                important_measures = \
                    list(self._annotate_measures(filtered_scan))

                if not any(
                        True for measure, importance in important_measures
                        if importance == self.MeasureImportance.STOP
                ):
                    self.stop_event.clear()
                else:
                    self.stop_event.set()

                    obstacles = self._measures_to_obstacles(important_measures)
                    pathfinder.path_obstacles = obstacles

                    continue

                if any(
                        True for measure, importance in important_measures
                        if importance == self.MeasureImportance.SLOW
                ):
                    self.slow_event.set()
                else:
                    self.slow_event.clear()

        finally:
            self.stop()

    def visualizer(self):
        from lidar.visualizer import Visualizer

        def loop(queue):
            time.sleep(5)

            for scan in self.lidar.iter_scans():
                queue.put(scan)

        queue = Queue()

        thread = threading.Thread(target=loop, args=(queue,))
        thread.start()

        Visualizer(queue=queue)

    def record(self, output_file):
        import json

        with open(output_file, 'w') as f:
            for scan in self.lidar.iter_scans():
                f.write(json.dumps(scan))

    def run_in_thread(self):
        thread = threading.Thread(target=self.mainloop, daemon=True)
        thread.start()

        return thread

    def stop(self):
        self.lidar.stop()
        self.lidar.stop_motor()
        self.lidar.disconnect()

    def _update_stop_shape(self):
        self._stop_shape = Ellipse(
            x=0,
            y=drive.brake_distance() / 2,

            rx=self._ROBOT_WIDTH / 2 + self._CLEARANCE +
            self.OPPONENT_RADIUS - self.BEACON_RADIUS,

            ry=drive.brake_distance() / 2 + self._CLEARANCE +
            self._ROBOT_LENGTH / 2 + self.OPPONENT_RADIUS -
            self.BEACON_RADIUS,
        )

    def _update_slow_shape(self):
        self._slow_shape = Ellipse(
            x=self._stop_shape.x,
            y=self._stop_shape.y,
            rx=self._stop_shape.rx + self._SLOW_OFFSET,
            ry=self._stop_shape.ry + self._SLOW_OFFSET,
        )


lidar = LidarManager(port='/dev/ttyUSB0')
