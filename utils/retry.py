class OutOfRetriesException(Exception):
    pass


class Retry:
    def __init__(self, retries):
        self.retries = retries
        # The first time it runs, it is on its 1-th try
        self.exception = None

    def __call__(self, f):
        def new_f(*args, **kwargs):

            for _ in range(self.retries):
                try:
                    return f(*args, **kwargs)
                except (IOError, OSError) as e:
                    self.exception = e
                    continue

            raise self.exception

        return new_f
