import math

from lib import Coord
from devices.secondary_devices import wheels


wheels.move_to(Coord(x=500, y=0, a=math.radians(90)))
wheels.move_to(Coord(x=500, y=1500, a=math.radians(180)))
wheels.move_to(Coord(x=0, y=1500, a=math.radians(-90)))
wheels.move_to(Coord(x=0, y=100, a=math.radians(-90)))
wheels.move_to(Coord(x=0, y=0, a=math.radians(90)))
