import itertools
from funcy import autocurry


fgetattr = autocurry(lambda key, o: getattr(o, key))


def pairwise(iterable):
    "s -> (s0,s1), (s1,s2), (s2, s3), ..."
    a, b = itertools.tee(iterable)
    next(b, None)
    return zip(a, b)


def triplets(iterable, include_ends=False):
    iterable = iter(iterable)

    try:
        a = None
        b = next(iterable)
        c = next(iterable)

        if include_ends:
            yield (a, b, c)

        while True:
            a = b
            b = c
            c = next(iterable)
            yield (a, b, c)

    except StopIteration:
        if include_ends:
            yield (b, c, None)


def insert(iterable, i, item):
    return iterable[:i] + [item] + iterable[i:]


def without(something, iterable):
    """
    Filter an iterable removing given item
    """

    return (x for x in iterable if x is not something)
