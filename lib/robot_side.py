from enum import IntEnum

from .table_side import TableSide
from .dotenv_manager import dotenv


TABLE_SIDE = TableSide(str(dotenv.get_value('TABLE_SIDE')))


class RobotSide(IntEnum):
    RIGHT = 1 if TABLE_SIDE == TableSide.YELLOW else -1
    LEFT = -1 if TABLE_SIDE == TableSide.YELLOW else 1
