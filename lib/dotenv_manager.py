import os
from dotenv import load_dotenv


class DotEnvValue:
    def __init__(self, key):
        self.key = key

    def __bool__(self):
        value = os.getenv(self.key)

        if value == 'true':
            return True
        if value == 'false':
            return False
        # TODO
        raise Exception()

    def __str__(self):
        return str(os.getenv(self.key))

    def __float__(self):
        return float(os.getenv(self.key))


class DotEnvManager:
    def __init__(self):
        self.is_loaded = False

    def load_dotenv(self):
        if self.is_loaded:
            return

        load_dotenv(dotenv_path=os.path.join(
            os.path.dirname(__file__),
            '../.env',
        ))

        self.is_loaded = True

    def get_value(self, key):
        self.load_dotenv()

        assert key in os.environ, \
            '`{key}` missing from `.env`.'.format(key=key)

        return DotEnvValue(key=key)


dotenv = DotEnvManager()
