import tkinter as tk
from PIL import Image, ImageTk


class ResizingCanvas(tk.Canvas):
    # pylint: disable=too-many-instance-attributes,too-many-ancestors

    def __init__(self, parent, width, height, bgimage=None, **kwargs):
        tk.Canvas.__init__(self, parent, width=width, height=height, **kwargs)
        self.bind('<Configure>', self.on_resize)
        self.height = self.winfo_reqheight()
        self.width = self.winfo_reqwidth()
        self.parent = parent
        self.x = 0
        self.y = 0
        self._GIVEN_WIDTH = width
        self._GIVEN_HEIGHT = height

        self.bgimage = None
        self.canvas_img = None

        if bgimage:
            self.bgimage = Image.open(bgimage)
            self.bgimage_tk = ImageTk.PhotoImage(self.bgimage)

            self.canvas_img = \
                self.create_image(0, 0, image=self.bgimage_tk, anchor='nw')

    def on_resize(self, event):
        new_height = int(min(
            (event.height + 2 * self.y),
            (event.width + 2 * self.x) *
            self._GIVEN_HEIGHT / self._GIVEN_WIDTH
        ))
        new_width = int(new_height * self._GIVEN_WIDTH / self._GIVEN_HEIGHT)

        scale = float(new_height) / self.height

        self.width = new_width
        self.height = new_height

        self.x = max(0, int((self.parent.winfo_width() - new_width) / 2))
        self.y = max(0, int((self.parent.winfo_height() - new_height) / 2))

        self.config(width=self.width, height=self.height)
        self.parent.config(padx=self.x, pady=self.y)

        self.scale('all', 0, 0, scale, scale)

        if self.bgimage:
            self.bgimage_tk = ImageTk.PhotoImage(self.bgimage.resize(
                (new_width, new_height),
                Image.ANTIALIAS,
            ))
            self.itemconfig(self.canvas_img, image=self.bgimage_tk)

    @property
    def current_scale(self):
        return max(
            self.width / self._GIVEN_WIDTH,
            self.height / self._GIVEN_HEIGHT,
        )

    def _create(self, itemType, args, kw):
        args = [arg * self.current_scale for arg in args]
        return super()._create(itemType, args, kw)

    def create_circle(self, x, y, r, *args, **kwargs):
        return self.create_oval(
            x - r,
            y - r,
            x + r,
            y + r,
            *args,
            **kwargs,
        )

    def event_coords(self, event):
        x = event.x * self._GIVEN_WIDTH / self.width
        y = event.y * self._GIVEN_HEIGHT / self.height

        return x, y
