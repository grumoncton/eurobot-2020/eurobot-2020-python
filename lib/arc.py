import math


class Arc:
    def __init__(self, p1, p2, center, intersection):
        self.p1 = p1
        self.p2 = p2
        self.center = center
        self.intersection = intersection

    @property
    def r(self):
        return math.hypot(
            self.p1.x - self.center.x,
            self.p1.y - self.center.y,
        )

    @property
    def length(self):
        return self.r * abs(
            math.atan2(self.p1.x - self.center.x, self.p1.y - self.center.y) -
            math.atan2(self.p2.x - self.center.x, self.p2.y - self.center.y)
        )

    def __str__(self):
        return 'Arc({}, {})'.format(self.p1, self.p2)
