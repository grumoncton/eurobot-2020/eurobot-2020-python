def fsetattr(path, d, value):
    if len(path) == 1:
        setattr(d, path[0], value)
        return

    fsetattr(path[1:], getattr(d, path[0]), value)
