import time
import math
import sched
import threading

from lib import Coord
from devices.encoders import encoders


class Positioning:
    PERIOD = 0.1

    def __init__(self):
        self.coord = Coord(x=450, y=165, a=math.radians(118))
        self.scheduler = sched.scheduler(time.time, time.sleep)

    def run_in_thread(self):

        # Set position to encoders so any movement before match start is reset
        encoders.set_position(self.coord)

        self.update()

        thread = threading.Thread(target=self.scheduler.run, daemon=True)
        thread.start()

        return thread

    def update(self):

        self.scheduler.enter(self.PERIOD, 1, self.update)

        # with Timeit('Update positon'):
        self.coord = encoders.get_position()

    @property
    def x(self):
        return self.coord.x

    @property
    def y(self):
        return self.coord.y

    @property
    def a(self):
        return self.coord.a
