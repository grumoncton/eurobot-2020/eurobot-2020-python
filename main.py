from lcd import lcd
from init import wait_until_ready, setup_logging
from init.init_sequence import run_init_sequence
from drive.differential import drive
from positioning import pos


def main():
    from lidar import lidar
    from task_manager import load_tasks
    from time_up_handler import start_timing

    setup_logging()

    lcd.display_string('Searching for odrive', line=1)
    lcd.display_string('Open E-Stop', line=2)

    task_manager = load_tasks()

    with lidar.init():
        lidar.run_in_thread()

        # Initialise robot
        run_init_sequence()
        wait_until_ready()

        pos.run_in_thread()

        lidar.stop_event.clear()

        start_timing()

        with drive.engage():
            task_manager.loop()


if __name__ == '__main__':
    main()
