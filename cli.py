#!venv/bin/python

import sys
import time
import click

from cli_commands.run_tests import test, lint
from cli_commands.markdown import markdown
from devices.secondary_mechanisms import \
    cli_commands as secondary_mechanisms_commands


@click.group()
def main():
    pass


main.add_command(secondary_mechanisms_commands.scara_move_to)


@main.command()
@click.option('--addr')
@click.option('--instruction')
@click.option('--args', required=False)
def send_instruction(addr, instruction, args):
    from comms import comms, BaseInstruction
    from devices import I2CDevice
    # import scanner.ports as ports
    # from scanner.device import Device

    device = I2CDevice()
    device.addr = int(addr, 0)

    try:
        instruction = int(instruction, 0)
    except ValueError:
        instruction = getattr(BaseInstruction, instruction.upper())
    # device = Device(port) if ports.is_port(port) \
    #     else ports.get_device_by_name(port)
    comms.send_instruction(
        device=device,
        instruction=instruction,
        args=args,
    )


@main.command()
@click.option('--addr')
@click.option('--instruction')
@click.option('--args', required=False)
def read_data(addr, instruction, args):
    from comms import comms, BaseInstruction
    from devices import I2CDevice
    # import scanner.ports as ports
    # from scanner.device import Device

    device = I2CDevice()
    device.addr = int(addr, 0)

    try:
        instruction = int(instruction, 0)
    except ValueError:
        instruction = getattr(BaseInstruction, instruction.upper())
    # device = Device(port) if ports.is_port(port) \
    #     else ports.get_device_by_name(port)
    click.echo(comms.read_data(
        device=device,
        instruction=instruction,
        args=args,
    ))


@main.command()
@click.argument('colour')
def to_opencv_colours(colour):
    import colorsys
    from colormap import hex2rgb

    r, g, b = hex2rgb(colour)
    h, s, v = colorsys.rgb_to_hsv(r=r, g=g, b=b)
    click.echo('h: {} s: {} v: {}'.format(h * 180, s * 255, v))


@main.command()
@click.argument('device')
def get_port(device):
    import scanner.ports as ports
    click.echo(device)
    click.echo(ports.get_device_by_name(sys.argv[2]).port)


@main.command('curses')
def enter_curses():
    import scanner.curses as curses
    curses.init()


@main.command('crc')
@click.argument('message')
def calc_crc(message):
    from comms.crc import crc16
    crc = crc16(message)
    click.echo(crc)
    click.echo(hex(crc))


@main.command('pathfinding-visualizer')
def pathfinding_visualizer():
    from pathfinder.visualizer import Visualizer

    Visualizer().mainloop()


@main.command('pathfinding-visualize-path')
@click.argument('origin')
@click.argument('destination')
@click.option('--output-file', default=None)
def pathfinding_visualize_path(origin, destination, output_file):
    import io
    from lib import Coord
    from pathfinder.visualizer import Visualizer
    from PIL import Image

    visualizer = Visualizer()
    visualizer.origin = Coord.from_string(origin)
    visualizer.destination = Coord.from_string(destination)
    visualizer.update_pathfinding()

    if output_file:
        visualizer.canvas.update()
        ps = visualizer.canvas.postscript(colormode='color')
        Image.open(io.BytesIO(ps.encode('utf-8'))).save(output_file, format='PNG')

    else:
        visualizer.mainloop()


@main.command()
def get_encoder_position():
    from devices import encoders

    click.echo(encoders.get_position())


@main.command()
@click.option('--host', default='barebones')
def deploy(host):
    import subprocess

    DIR = '~/code/eurobot2020/python/'

    subprocess.call([
        'ssh',
        host,
        'mkdir',
        '-p',
        DIR,
    ])

    rsync = next(
        filename for filename in ('/usr/bin/rsync', 'C:/DeltaCopy/rsync.exe')
        if os.path.isfile(filename)
    )

    subprocess.call([
        rsync,
        '-acv',
        '--no-perms',
        '--omit-dir-times',
        '--delete',
        '--stats',
        '--progress',
        '--exclude=venv',
        '--exclude=__pycache__',
        '--exclude=source',
        '--exclude=storage',
        '--exclude=.git',
        '--exclude=.env',
        '--exclude=*.json',
        '--exclude=*.swp',
        '--exclude=tags',
        './',
        '{host}:{DIR}'.format(host=host, DIR=DIR),
    ])


@main.command()
def lidar_visualizer():
    from lidar.lidar import LidarManager

    lidar = LidarManager(port='/dev/ttyUSB0')
    with lidar.init():
        lidar.visualizer()


@main.command()
@click.option('--output-file', '-f', required=True)
def lidar_recorder(output_file):
    from lidar.lidar import LidarManager

    lidar = LidarManager(port='/dev/ttyUSB0')
    with lidar.init():
        time.sleep(5)
        lidar.record(output_file)


main.add_command(test)
main.add_command(lint)
main.add_command(markdown)


if __name__ == '__main__':
    from init.setup_logging import setup_logging

    setup_logging()
    main()
