import re
import time
from watchdog.events import FileSystemEventHandler


class MarkdownFileChangeHandler(FileSystemEventHandler):
    def __init__(self, callback, throttle, include, exclude):
        super(MarkdownFileChangeHandler, self).__init__()

        self.callback = callback
        self.throttle = throttle
        self.last_ran = time.time()
        self.include = re.compile(include)
        self.exclude = re.compile(exclude)

    def on_modified(self, event):
        current_time = time.time()

        if self.last_ran + self.throttle > current_time \
                or self.exclude.search(event.src_path) \
                or not self.include.search(event.src_path):
            return

        self.last_ran = current_time
        self.callback()
