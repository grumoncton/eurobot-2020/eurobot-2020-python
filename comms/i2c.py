import logging

from lib import dotenv


logger = logging.getLogger(__name__)
HAS_I2C = dotenv.get_value('HAS_I2C')


class I2C:
    def __init__(self, interface):
        from . import BaseInstruction

        if not HAS_I2C:
            from unittest.mock import MagicMock
            self.bus = MagicMock()
            self.bus.read_i2c_block_data.return_value = \
                [BaseInstruction.ACK, 0xc6, 0x60]
            return

        from smbus2 import SMBus  # pylint: disable=import-error
        self.bus = SMBus(interface)

    def write_quick(self, addr):
        return self.bus.write_quick(addr)

    def write_byte(self, addr, byte):
        # TODO: Retries and shit maybe?
        return self.bus.write_byte(addr, byte)

    def write_byte_data(self, addr, cmd, byte):
        # TODO: Retries and shit maybe?
        return self.bus.write_byte_data(addr, cmd, byte)

    def read_block(self, addr, instruction):
        from . import FRAME_LENGTH

        return self.bus.read_i2c_block_data(
            addr,
            instruction,
            FRAME_LENGTH,
        )

    def read_byte_data(self, addr, cmd):
        return self.bus.read_byte_data(addr, cmd)

    def write_block(self, addr, instruction, args):
        logger.debug(
            'Sending instruction 0x%(instruction)x to 0x%(addr)x with args '
            '%(args)s.',
            {
                'addr': addr,
                'instruction': instruction,
                'args': ':'.join('{0:x}'.format(b) for b in args),
            },
        )
        return self.bus.write_i2c_block_data(addr, instruction, list(args))
