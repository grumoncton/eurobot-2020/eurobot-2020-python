from .i2c import I2C
from .base_instruction import BaseInstruction
from .comms import send_instruction, read_data
from .status import CommsStatus

i2c = I2C(interface=1)

FRAME_LENGTH = 31
CRC_LENGTH = 2
ARG_LENGTH = FRAME_LENGTH - CRC_LENGTH

__all__ = ('FRAME_LENGTH', 'CRC_LENGTH', 'ARG_LENGTH', 'I2C', 'i2c',
           'BaseInstruction', 'send_instruction', 'read_data', 'CommsStatus')
