import uuid
from unittest import TestCase
from unittest.mock import MagicMock

from .serial_utils import OpenSerialPort


class SerialUtilsTestCase(TestCase):
    @staticmethod
    def test_opens_and_closes_serial_port():
        interface = MagicMock()
        interface.is_open = False

        with OpenSerialPort(interface):
            interface.open.assert_called_once()
            interface.close.assert_not_called()

        interface.close.assert_called_once()

    @staticmethod
    def test_only_opens_once():
        interface = MagicMock()
        interface.is_open = True

        with OpenSerialPort(interface):
            interface.open.assert_not_called()
            interface.close.assert_not_called()

    def test_sets_port(self):
        interface = MagicMock()
        port = uuid.uuid4()

        with OpenSerialPort(interface, port):
            self.assertEqual(interface.port, port)
