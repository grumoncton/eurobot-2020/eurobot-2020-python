import itertools
from unittest import TestCase
from unittest.mock import MagicMock, patch, call

from utils.retry import OutOfRetriesException
from . import MESSAGE_TIMEOUT, MAX_RETRIES, MESSAGE_ACK, MESSAGE_RESEND
from .comms import _readline, _parse_response, _send_message, \
    _send_message_and_get_response, send_command
from .crc import InvalidChecksumException


class ReadlineTestCase(TestCase):
    def test_readline(self):
        mocked_device = MagicMock()
        mocked_device.ser.readline.return_value = \
            [72, 101, 108, 108, 111, 32, 119, 111, 114, 108, 100, 33, 10]

        res = _readline(mocked_device)

        self.assertTrue(res, 'Hello world')

    @staticmethod
    @patch('utils.timeout.Timeout')
    def test_timeout(mocked_timeout):
        mocked_device = MagicMock()
        _readline(mocked_device)
        mocked_timeout.asser_called_once_with(MESSAGE_TIMEOUT)


class ParseResponseTestCase(TestCase):
    def test_separates_message(self):
        message = 'Hello world!{}{}'.format(chr(0xdb), chr(0x39))

        command = _parse_response(message)

        self.assertEqual(command, 'Hello world!')

    def test_checksum_error(self):
        message = 'Message without checksum'

        with self.assertRaises(InvalidChecksumException):
            _parse_response(message)


class SendMessageWithCrcTestCase(TestCase):

    @staticmethod
    def test_sends_message():
        mocked_device = MagicMock()
        message = 'Hello world!'

        _send_message(mocked_device, message)

        mocked_device.ser.write.asser_called_once_with(
            [72, 101, 108, 108, 111, 32, 119, 111, 114, 108, 100, 33, 10])

    def test_message_length(self):
        with self.assertRaises(AssertionError):
            _send_message(device=None, command=' ' * 100)


class SendMessageTestCase(TestCase):

    @patch('comms.comms._readline')
    @patch('comms.comms._send_message')
    def test_sends_message(self, mocked_send_message_with_crc,
                           mocked_readline):
        mocked_device = MagicMock()
        message = 'Hello world!'

        res = _send_message_and_get_response(mocked_device, message)

        self.assertEqual(res, mocked_readline.return_value)
        mocked_send_message_with_crc.assert_called_once_with(mocked_device,
                                                             message)

    @staticmethod
    @patch('comms.comms.Retry')
    def test_retries(mocked_retry):
        mocked_device = MagicMock()

        _send_message_and_get_response(mocked_device, '')

        mocked_retry.assert_called_once_with(MAX_RETRIES)


class SendCommandTestCase(TestCase):

    @patch('comms.comms._send_message')
    @patch('comms.comms._parse_response')
    @patch('comms.comms._send_message_and_get_response')
    def test_happy_path(self, mocked_send_message_and_get_response,
                        mocked_parse_response, mocked_send_message):
        mocked_device = MagicMock()
        message = 'Hello world!'

        res = send_command(mocked_device, message)

        mocked_send_message_and_get_response.asser_called_once_with(
            mocked_device, message)
        mocked_send_message.assert_called_once_with(mocked_device, MESSAGE_ACK)
        self.assertEqual(res, mocked_parse_response.return_value)

    @staticmethod
    @patch('comms.comms.print', MagicMock)
    @patch('comms.comms._send_message')
    @patch('comms.comms._parse_response')
    @patch('comms.comms._send_message_and_get_response')
    def test_asks_for_resend_on_crc_error(
            mocked_send_message_and_get_response,
            mocked_parse_response,
            mocked_send_message):

        message = ''
        mocked_device = MagicMock()

        # First call to `parse_response` will throw `InvalideChecksumException`
        # Subsequent calls will return message
        mocked_parse_response.side_effect = \
            itertools.chain(iter([InvalidChecksumException()]),
                            itertools.repeat(message))

        mocked_send_message.assert_not_called()

        send_command(mocked_device, message)

        mocked_send_message_and_get_response.assert_has_calls([
            call(mocked_device, message),
            call(mocked_device, MESSAGE_RESEND),
            call(mocked_device, message),
        ])

        mocked_send_message.assert_called_once_with(mocked_device, MESSAGE_ACK)

    @staticmethod
    @patch('comms.comms._parse_response')
    @patch('comms.comms._send_message_and_get_response')
    def test_resends_when_asked(mocked_send_message_and_get_response,
                                mocked_parse_response):
        message = 'Hello world!'
        mocked_device = MagicMock()
        # Set first return value to `RESEND`
        # Subsequent return values will be `ACK`
        mocked_parse_response.side_effect = \
            itertools.chain(iter([MESSAGE_RESEND]),
                            itertools.repeat(MESSAGE_ACK))

        send_command(mocked_device, message)

        mocked_send_message_and_get_response.assert_has_calls(
            [call(mocked_device, message)] * 2)

    @patch('comms.comms.print', MagicMock)
    @patch('comms.comms._parse_response')
    @patch('comms.comms._send_message_and_get_response', MagicMock)
    def test_max_retries(self, mocked_parse_response):
        mocked_parse_response.side_effect = InvalidChecksumException()

        with self.assertRaises(OutOfRetriesException):
            send_command(device=MagicMock(), command='')
