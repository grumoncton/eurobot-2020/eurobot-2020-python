from unittest import TestCase

from .crc import crc16


class CrcTestCase(TestCase):
    def test_calculate_crc(self):
        res = crc16('Hello world!')
        self.assertEqual(res, 14811)
