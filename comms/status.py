from enum import IntEnum


class CommsStatus(IntEnum):
    INITIALISING = 0
    READY = 1
    WORKING = 2
