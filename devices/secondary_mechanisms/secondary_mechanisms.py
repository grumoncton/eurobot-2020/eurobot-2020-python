from devices import I2CStatusDevice


class SecondaryMechanisms(I2CStatusDevice):
    addr = 0x41
    name = 'SecondaryMechanisms'


secondary_mechanisms_device = SecondaryMechanisms()
