import struct

from lib import TableSide, dotenv
from task_manager import BaseStep
from comms import send_instruction
from . import SecondaryMechanismsInstruction, secondary_mechanisms_device


TABLE_SIDE = TableSide(str(dotenv.get_value('TABLE_SIDE')))


class ScaraMoveToStep(BaseStep):

    name = 'ScaraMoveToStep'

    def __init__(self, position):
        self.position = position if TABLE_SIDE == TableSide.YELLOW \
            else position.to_prime()
        self.device = secondary_mechanisms_device
        self.instruction = SecondaryMechanismsInstruction.SCARA_MOVE_TO
        super().__init__()

    def run(self):
        args = struct.pack(
            '<ff',
            float(self.position.x),
            float(self.position.y),
        )

        send_instruction(
            device=self.device,
            instruction=self.instruction,
            args=args,
        )


class ScaraStashStep(BaseStep):

    name = 'ScaraStashStep'

    def __init__(self, side):
        self.side = side
        self.device = secondary_mechanisms_device
        self.instruction = SecondaryMechanismsInstruction.SCARA_STASH
        super().__init__()

    def run(self):
        send_instruction(
            device=self.device,
            instruction=self.instruction,
            args=struct.pack('b', self.side.value),
        )


class ScaraSpoonStep(BaseStep):

    name = 'ScaraSpoonStep'

    def __init__(self, side):
        self.side = side
        self.device = secondary_mechanisms_device
        self.instruction = SecondaryMechanismsInstruction.SCARA_SPOON
        super().__init__()

    def run(self):
        send_instruction(
            device=self.device,
            instruction=self.instruction,
            args=struct.pack('b', self.side.value),
        )
