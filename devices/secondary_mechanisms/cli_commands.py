import click


@click.command()
@click.argument('x')
@click.argument('y')
def scara_move_to(x, y):
    from .scara_steps import ScaraMoveToStep

    ScaraMoveToStep(x=x, y=y).run()
