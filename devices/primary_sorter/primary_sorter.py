from devices import I2CStatusDevice


class PrimarySorter(I2CStatusDevice):
    addr = 0x46
    name = 'PrimarySorter'
