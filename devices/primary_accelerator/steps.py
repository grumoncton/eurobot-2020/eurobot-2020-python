from task_manager import SimpleStep
from . import primary_accelerator_device, PrimaryAcceleratorInstruction


class RaiseTilterStep(SimpleStep):
    """
    Raise accelerator tilter.
    """

    device = primary_accelerator_device
    instruction = PrimaryAcceleratorInstruction.TILTER_RAISE


class LowerTilterStep(SimpleStep):
    """
    Lower accelerator tilter.
    """

    device = primary_accelerator_device
    instruction = PrimaryAcceleratorInstruction.TILTER_LOWER


class SorterSortRightStep(SimpleStep):
    """
    Sort the puck to the right.
    """

    device = primary_accelerator_device
    instruction = PrimaryAcceleratorInstruction.STEPPER_SORT_RIGHT


class SorterSortLeftStep(SimpleStep):
    """
    Sort the puck to the left.
    """

    device = primary_accelerator_device
    instruction = PrimaryAcceleratorInstruction.STEPPER_SORT_LEFT
