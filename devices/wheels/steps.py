from lib import RobotSide
from pathfinder import pathfinder
from task_manager import BaseStep
from .wheels import Wheels


class MoveToStep(BaseStep):
    """
    Step commanding the wheels to move to given coordinates.
    """

    name = 'MoveToStep'

    def __init__(self, destination):
        self.destination = destination

        super().__init__()

    def run(self):
        from devices.secondary_devices import wheels

        self.logger.info('(%s) Moving to %s', self.name, self.destination)
        wheels.move_to(destination=self.destination)


class RotateToStep(BaseStep):
    """
    Step commanding the wheels to rotate to given heading.
    """

    name = 'RotateToStep'

    def __init__(self, heading):
        self.heading = heading

        super().__init__()

    def run(self):
        from devices.secondary_devices import wheels

        self.logger.info('(%s) Rotating to %f.', self.name, self.heading)
        wheels.rotate_to(heading=self.heading)


class RotateAroundPointStep(BaseStep):
    """
    Step commanding the wheels to do cuviligne rotation.
    """

    name = 'RotateAroundPointStep'

    def __init__(self, heading, side, radius):
        self.heading = heading
        self.side = side
        self.radius = radius

        super().__init__()

    def run(self):
        from devices.secondary_devices import wheels

        self.logger.info(
            '(%s) Rotating to %f with radius %f.',
            self.name,
            self.heading,
            self.radius
        )
        wheels.rotate_around_point(
            heading=self.heading,
            side=self.side,
            radius=self.radius,
        )


class PathfindingMoveToStep(BaseStep):
    """
    Step to command the wheels to move to given coordinates using pathfinding.
    """

    name = 'PathfindingMoveToStep'

    def __init__(self, destination):
        self.destination = destination
        super().__init__()

    def run(self):
        from devices.secondary_devices import wheels

        path = pathfinder.shortest_path(origin=wheels.pos,
                                        destination=self.destination)

        for point in path:
            self.logger.info('(%s) Moving to %s', self.name, self.destination)
            wheels.move_to(destination=point)


class DriveStraightStep(BaseStep):
    """
    Drive straight relative to current position.
    """

    name = 'DriveStraightStep'

    def __init__(self, distance, direction=Wheels.Direction.FORWARDS):
        self.distance = distance
        self.direction = direction

        super().__init__()

    def run(self):
        from devices.secondary_devices import wheels

        wheels.drive_straight(
            distance=self.distance,
            direction=self.direction,
        )


class ChangeConfigStep(BaseStep):
    """
    Differential drive step to change odrive configuration in context manager.
    """

    name = 'ChangeConfigStep'

    def __init__(
            self,
            step,
            speed=Wheels.DRIVE_SPEED,
            accel=Wheels.DRIVE_ACCEL,
            decel=Wheels.DRIVE_DECEL,
    ):
        self.step = step
        self.speed = speed
        self.accel = accel
        self.decel = decel

        super().__init__()

    def run(self):
        from devices.secondary_devices import wheels

        with wheels.change_config():
            for side in RobotSide:
                wheels.configure_axis(
                    side=side,
                    speed=self.speed,
                    accel=self.accel,
                    decel=self.decel,
                )

            self.step.run()
