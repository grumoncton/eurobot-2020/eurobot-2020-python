import time
from lib import Coord
from lidar import lidar
from devices.secondary_devices import wheels


try:
    lidar.init()
    lidar.run_in_thread()

    # This can be removed for a real match
    # The carrot will be in the robot the whole time the lidar spools up
    time.sleep(5)

    with wheels.engage():
        wheels.move_to(Coord(x=0, y=2000))
        wheels.move_to(
            Coord(x=0, y=0),
            direction=wheels.Direction.BACKWARDS,
        )

finally:
    lidar.stop()
