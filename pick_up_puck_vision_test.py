# import sys
# import time
# from init.setup_logging import setup_logging
# from vision import find_blue, find_green
# from devices.secondary_mechanisms.scara_steps import ScaraMoveToStep, \
#     ScaraStashStep
# from devices.secondary_mechanisms.steps import EnableVacuumStep, \
#     DisableVacuumStep
# from devices.secondary_mechanisms.leadscrew import LeadscrewStep, LeadscrewState

# setup_logging()

# put_it_left = False

# while True:
#     # ScaraStashStep(position=ScaraStashStep.Position.RIGHT).run()
#     ScaraMoveToStep(x=155, y=10).run()

#     blue_pucks = find_blue()
#     print('blue', blue_pucks)

#     if not blue_pucks:
#         break

#     blue_puck = blue_pucks[0]

#     if blue_puck[1] < 40:
#         print("Too close. Must have picked up robot")
#         continue

#     ScaraMoveToStep(*blue_puck).run()

#     EnableVacuumStep().run()
#     LeadscrewStep(state=LeadscrewState.HOME).run()
#     LeadscrewStep(state=LeadscrewState.CLEAR_STASH_HEIGHT).run()

#     ScaraStashStep(
#         position=ScaraStashStep.Position.RIGHT if not put_it_left
#         else ScaraStashStep.Position.LEFT,
#     ).run()
#     time.sleep(1)
#     LeadscrewStep(state=LeadscrewState.STASH_HEIGHT).run()
#     DisableVacuumStep().run()
#     LeadscrewStep(state=LeadscrewState.CLEAR_STASH_HEIGHT).run()
#     put_it_left = True

#     # print('blue', find_blue())
# # print('green', find_green())

import sys

from lib import RobotSide
from init.setup_logging import setup_logging
from vision import find_blue, find_green, PickUpCaosPuckWithVision


setup_logging()

import time
from lib import Coord
from devices.secondary_devices import wheels
from devices.secondary_mechanisms.steps import EnableVacuumStep, \
    DisableVacuumStep
from devices.secondary_mechanisms.leadscrew import LeadscrewStep, LeadscrewState
from devices.secondary_mechanisms.scara_steps import ScaraMoveToStep

# EnableVacuumStep().run()
with wheels.engage():
    try:
        wheels.move_to(Coord(x=0, y=2000))
        # DisableVacuumStep().run()
        # ScaraMoveToStep(Coord(x=160, y=0)).run()
        # wheels.move_to(Coord(x=0, y=1400))

    finally:
        time.sleep(100e3)
# LeadscrewStep(LeadscrewState.PUCK_PICK_UP_HEIGHT).run()

sys.exit()


PickUpCaosPuckWithVision(
    side=RobotSide.RIGHT,
    puck_finder=find_blue,
).run()

PickUpCaosPuckWithVision(
    side=RobotSide.LEFT,
    puck_finder=find_green,
).run()
