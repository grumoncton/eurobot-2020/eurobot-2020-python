import logging
import itertools

from lcd import lcd
from lidar import lidar
from lib import TableSide, dotenv


logger = logging.getLogger(__name__)

TABLE_SIDE = TableSide(str(dotenv.get_value('TABLE_SIDE')))


def wait_until_ready():
    from . import GPIO, get_disconnected_devices
    from .GPIO import CARROT_PIN

    if GPIO is None:
        return

    carrot_inserted = False

    # this loop will wait utile the carrot is pulled
    for i in itertools.count():
        if not GPIO.input(CARROT_PIN) and not carrot_inserted:
            lcd.display_string('Insert carrot', line=1)
            continue

        else:
            carrot_inserted = True

        if i % 1000 == 0:
            disconnected_devices = get_disconnected_devices()

            # If devices are disconnected, try again
            if disconnected_devices:
                message = '%(device)s disconnected.' % {
                    'device': str(disconnected_devices[0]),
                }
                logger.warning(message)

                lcd.display_string(message, line=1)

                continue

            if not lidar.ready.is_set():
                lcd.display_string('Waiting for lidar', line=1)
                continue

            lcd.clear()
            lcd.display_string(TABLE_SIDE.name, line=1)

            lcd.display_string(
                'Voltage: %.2fV' % drive.vbus_voltage,
                line=2,
            )

        # Exit when carrot removed
        if not GPIO.input(CARROT_PIN):
            break
