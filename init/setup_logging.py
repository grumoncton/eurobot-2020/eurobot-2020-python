def setup_logging():
    import logging
    import logging.config

    logging.config.dictConfig({
        'version': 1,
        'disable_existing_loggers': False,
        'formatters': {
            'standard': {
                'format': '%(asctime)s [%(levelname)s] %(name)s: %(message)s'
            },
        },
        'handlers': {
            'default_handler': {
                'class': 'logging.StreamHandler',
                'level': 'DEBUG',
                'formatter': 'standard',
            },
        },
        'loggers': {
            '': {
                'handlers': ['default_handler'],
                'level': 'WARNING',
                'propagate': False
            },
            'shapely.geos': {
                'level': 'WARNING',
            },
            'PIL': {
                'level': 'WARNING',
            },
            'rplidar': {
                'level': 'WARNING',
            },
            'comms.comms': {
                'level': 'WARNING',
            },
            'comms.i2c': {
                'level': 'WARNING',
            },
            'init.table_side': {
                'level': 'INFO',
            },
            'devices.wheels.wheels': {
                'level': 'DEBUG',
            },
            'devices.wheels.odrive_connector': {
                'level': 'WARNING',
            },
            'devices.encoders.encoders': {
                'level': 'WARNING',
            },
            'matplotlib': {
                'level': 'WARNING',
            },
            'vision.steps': {
                'level': 'DEBUG',
            },
            'lcd.lcd': {
                'level': 'WARNING',
            },
        }
    })
    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)
