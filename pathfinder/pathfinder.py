import math
from enum import IntEnum
from shapely.geometry import LineString, Point

from lib import Coord, Arc
from lib.functional import pairwise, triplets, insert, without
from .obstacle import Circle


class InvalidPathError(RuntimeError):
    pass


class Side(IntEnum):
    ABOVE = -1
    BELOW = 1


def getExtrapoledLine(p1, p2):
    """
    Creates a line extrapoled in p1->p2 direction
    """

    EXTRAPOL_RATIO = 200
    if issubclass(type(p1), Point):
        p1 = (p1.x, p1.y)

    if issubclass(type(p2), Point):
        p2 = (p2.x, p2.y)

    a = p1
    b = (
        p1[0] + EXTRAPOL_RATIO * (p2[0] - p1[0]),
        p1[1] + EXTRAPOL_RATIO * (p2[1] - p1[1]),
    )
    return LineString([a, b])


def getDoubleExtrapoledLine(p1, p2):
    """
    Like `getExtrapoledLine` but extrapolates in both directions
    """

    # Flip arguments here because they are flipped below
    # Don't want to change order of points
    line = getExtrapoledLine(p1=p2, p2=p1)

    # Second order flip here
    return getExtrapoledLine(line.coords[1], line.coords[0])


class Pathfinder:

    TABLE_WIDTH = 3000
    TABLE_HEIGHT = 2000

    def __init__(self):

        # Game elements
        self.permanent_obstacles = [
            # Roches
            Circle(x=2000, y=900, r=150),
            Circle(x=2000, y=2100, r=150),
            Circle(x=2000, y=1500, r=300),
        ]

        # Dynamic obstacles that are detected during the current path (other
        # robots)
        self.path_obstacles = [
            Circle(x=500, y=500, r=200),
            Circle(x=1000, y=1500, r=500),
        ]

    @property
    def obstacles(self):
        """
        Helper property to get both permanent obstacles and path obstacles
        """

        return self.permanent_obstacles + self.path_obstacles

    @staticmethod
    def dist_two_points(a, b):
        """
        Get the distance between two points. Basically just calculate the
        hypotenuse.
        """

        p1 = Point(a[0], a[1]) if isinstance(a, tuple) else \
            a.p2 if isinstance(a, Arc) else a

        p2 = Point(b[0], b[1]) if isinstance(b, tuple) else \
            b.p1 if isinstance(b, Arc) else b

        return math.hypot(p1.x - p2.x, p1.y - p2.y) + \
            (a.length if isinstance(a, Arc) else 0)

    @staticmethod
    def inside_tangent(a, b):
        """
        Get the point that is the intersection of the inside tangents of two
        circles. Inside tangent meaning the one that goes above one circle and
        below the other, not the one that goes above or below both. This point
        can be used to calculate the tangent lines using the same point-circle
        algorithm used elsewhere. That's why we return the point and not the
        line.

        https://stackoverflow.com/questions/27970185/find-line-that-is-tangent-to-2-given-circles
        """

        # Calculate the ratio of the radii
        # This is also the ratio of the distances from the wanted point and the
        # center of each circle
        m = a.r / b.r

        # Calculate circle distance
        d = math.hypot(a.x - b.x, a.y - b.y)

        # Calculate distances from wanted point to each circle
        h1 = m * d / (1 + m)
        h2 = d / (1 + m)

        # Intersection point calculated by interpolating between the centers
        return Coord(
            x=(h2 * a.x + h1 * b.x) / d,
            y=(h2 * a.y + h1 * b.y) / d,
        )

    @staticmethod
    def outside_tangent(a, b, side):
        """
        Get the line that is the outside tangent to a and b on the given side.
        Outside tangent meaning it goes above both circles or below both
        circles.

        http://jwilson.coe.uga.edu/emt669/Student.Folders/Kertscher.Jeff/Essay.3/Tangents.html
        """

        # Use a different method if the circles are the same size
        # Avoid division by zero error
        if a.r == b.r:
            r = a.r

            # Get angle perpendicular to the line between the two circles
            # Add or subtract 90 degrees based on side
            angle = math.atan2((a.y - b.y), (a.x - b.x)) - math.pi / 2 * side

            # Calculate the tangent point on both circles using the radius
            p1 = Point(a.x + r * math.cos(angle), a.y + r * math.sin(angle))
            p2 = Point(b.x + r * math.cos(angle), b.y + r * math.sin(angle))

            # Extend line in each direction to make sure it hits it's
            # neighbouring tangent lines
            return getDoubleExtrapoledLine(p1, p2), p1

        # Figure out which circle is the big one and which is the small one
        (small, big) = sorted((a, b), key=lambda x: x.r)

        # Flip side if sorting changed order
        if (small, big) != (a, b):
            side *= -1

        # Get distance between circles
        d = math.hypot(small.x - big.x, small.y - big.y)

        # Find circle that is centered on the big circle and has a radius of
        # big minus small
        medium = big.center.buffer(big.r - small.r).boundary

        # Find a circle that intercepts the centers of big and small
        # This circles is halfway between the two and has radius d / 2
        middle = Point(
            small.x + (big.x - small.x) / 2,
            small.y + (big.y - small.y) / 2,
        ).buffer(d / 2).boundary

        # Find the intersection of the inscribed circle in big and the circle
        # that touches big and small
        # This intersection will give us the angle of the tangent point
        # The intersection of two circles will give two results. Pick the
        # right one based on the wanted side
        intersection = medium.intersection(middle)[
            1 if side == Side.ABOVE else 0
        ]

        # Calculate the angle between the big point and the intersection point
        angle = math.atan2(
            intersection.y - big.y,
            intersection.x - big.x,
        )

        # Find the tangent point on the big circle
        big_point = Point(
            big.x + big.r * math.cos(angle),
            big.y + big.r * math.sin(angle),
        )

        # Find the tangent point on the small circle
        small_point = Point(
            small.x + small.r * math.cos(angle),
            small.y + small.r * math.sin(angle),
        )

        # Extend line in each direction to make sure it hits it's
        # neighbouring tangent lines
        return getDoubleExtrapoledLine(big_point, small_point), \
            small_point if (small, big) == (a, b) else big_point

    def find_tangent(self, circle, point, side, flip_side):
        """
        Find tangent points in a circle from a point:
        https://stackoverflow.com/questions/49968720/find-tangent-points-in-a-circle-from-a-point
        """

        # Have to use ugly flag because we only want to flip side if it's the
        # line to a point
        orig_side = side
        if flip_side:
            side *= -1

        # If the point is actually an obstacle-side tuple, we are dealing with
        # two circles
        # Use one of the two circle algorithms (inside or outside)
        if not isinstance(point, Coord):
            other_circle, other_side = point

            # Even though `side` determines the side the lines go on,
            # `orig_side` should be used to check whether to use inside tangent
            # or outside tangent
            if orig_side == other_side:
                line = self.outside_tangent(circle, other_circle, side=side)
                return line
            else:
                point = self.inside_tangent(circle, other_circle)

        # Point-circle algorithm
        # The point may have come from the inside tangent method

        # Find the distance between the circle and the point
        dist_to_circle_center = self.dist_two_points(point, circle)

        # Angle between the line from the circle center to where the tangent
        # line intercepts and the line between the circle center and the point
        # Changes sign based on side
        theta = math.acos(circle.r / dist_to_circle_center) * side

        # Angle between the point and the circle
        # Used to calculate the angle between the circle center and the tangent
        # point in absolute coordinates
        alpha = math.atan2(
            (point.y - circle.y),
            (point.x - circle.x),
        )

        # Get the tangent point in absolute coordinates
        tangent_point = Point(
            circle.x + circle.r * math.cos(theta + alpha),
            circle.y + circle.r * math.sin(theta + alpha),
        )

        # Extrapolate the line to ensure it hits neighbouring tangents
        line = getExtrapoledLine(point, tangent_point)

        return line, tangent_point

    def ensure_point_valid(self, point):
        """
        Raise an error if a point is not "valid"

        For now, that means that the point is on the table.
        """

        if 0 < point.x < self.TABLE_HEIGHT and \
                0 < point.y < self.TABLE_WIDTH:
            return point

        raise InvalidPathError()

    def with_obstacles_to_points(self, path, optimise_with_arcs=False):
        """
        Determine a list of points based on a list of points and obstacles. We
        need to keep a list of points and obstacles because the point generated
        by an obstacle depends on its neighbouring obstacles.
        """

        # The origin can't be an obstacle
        yield path[0]

        # Loop through each point / obstacle considering its predecessor and
        # its successor
        for before, curr, after in triplets(path):

            # If it's simple point, yield it
            if isinstance(curr, Coord):
                yield curr
                continue

            # Otherwise, it's a obstacle-side tuple
            obstacle, side = curr
            if isinstance(obstacle, Circle):

                # Find the tangent line between the previous point / obstacle
                # and the current obstacle
                t1, p1 = self.find_tangent(
                    circle=obstacle,
                    point=before,
                    side=side,
                    flip_side=False,
                )

                # Find the tangent line between the next point / obstacle
                # and the current obstacle
                # Flip the side, because the side depends on the direction
                t2, p2 = self.find_tangent(
                    circle=obstacle,
                    point=after,
                    side=side,
                    flip_side=True,
                )

                # Get the intersection of the two tangents
                intersection = t1.intersection(t2)

                # If the tangent does not exist, the path is not valid
                if not isinstance(intersection, Point):
                    raise InvalidPathError()

                # If not optimising, return the intersection
                if not optimise_with_arcs:
                    yield intersection
                    continue

                # Use the law of cosines to find the angle between the tangent
                # lines
                a = self.dist_two_points(intersection, p1)
                b = self.dist_two_points(intersection, p2)
                c = self.dist_two_points(p1, p2)

                angle = math.acos((a**2 + b**2 - c**2) / (2 * a * b))

                # If the angle is obtuse, the intersection is already close to
                # the circle
                # Don't optimise with arc
                if angle > math.pi / 2:
                    yield intersection
                    continue

                # Replace the intersection with and arc that follows the circle
                # circumference
                yield Arc(
                    p1=p1,
                    p2=p2,
                    center=obstacle.center,
                    intersection=intersection,
                )

        # The destination can't be an obstacle
        yield path[-1]

    @staticmethod
    def should_exclude_point(prev_point, curr_point, next_point, point):
        """
        Determine if a point is needed to correctly avoid all obstacles. This
        is basically a filter to remove obstacles that pull the path to them
        when the surrounding obstacles already ensure that the path misses this
        obstacle.
        """

        # Skip if current point isn't an obstacle
        if isinstance(point, Coord):
            return False

        obstacle, _ = point

        # If there is no intersection between the line between the current
        # point passing through the obstacle center and the line between the
        # last point and the previous point, then the surrounding obstacles
        # pull the path away from the current obstacle enough and the current
        # obstacle doesn't need to be considered
        return getExtrapoledLine(curr_point, obstacle.center) \
            .intersection(LineString([prev_point, next_point])).is_empty

    def expand_path(self, path, obstacles):
        """
        Expand the given path by adding a point to avoid any obstacle it hits.
        Return the path unaltered otherwise.
        """

        try:
            # Get the points that make up the path with obstacles
            path_as_points = list(self.with_obstacles_to_points(path))
        except InvalidPathError:
            return []

        # Look for points that should be excluded
        for (prev_point, curr_point, next_point), point \
                in zip(triplets(path_as_points, include_ends=True), path):

            # If a point should be excluded, return result of recursive call
            # without point
            if self.should_exclude_point(
                    prev_point=prev_point,
                    curr_point=curr_point,
                    next_point=next_point,
                    point=point,
            ):
                obstacle, _ = point

                return self.expand_path(
                    path=list(without(point, path)),
                    obstacles=obstacles,
                )

        # Loop through each segment (pair of points) in the path
        for i, segment in enumerate(pairwise(path_as_points)):
            try:

                # Get the first obstacle in the path
                blocking = next(
                    obstacle for obstacle in obstacles
                    if obstacle.is_blocking_segment(segment)
                )

            # If no obstacle blocks the segment, try the next
            except StopIteration:
                continue

            # If the is an obstacle, recurse, avoiding it above and below
            # The obstacle has been considered and should be removed from
            # recursive call
            return [
                *self.expand_path(
                    path=insert(path, i + 1, (blocking, Side.ABOVE)),
                    obstacles=list(without(blocking, obstacles)),
                ),
                *self.expand_path(
                    path=insert(path, i + 1, (blocking, Side.BELOW)),
                    obstacles=list(without(blocking, obstacles)),
                ),
            ]

        try:

            # Ensure that all points in path are valid
            for point in path_as_points:
                self.ensure_point_valid(point)

            # Return the result of optimising the path with arcs
            # Return list of list because this method should always return a
            # list of paths
            return [list(self.with_obstacles_to_points(
                path=path,
                optimise_with_arcs=True,
            ))]

        except InvalidPathError:
            return []

    def shortest_path(self, origin, destination):
        """
        Find the shortest path between the origin and the destination
        """

        paths = self.expand_path(
            path=[origin, destination],
            obstacles=self.obstacles,
        )

        shortest = sorted(paths, key=lambda path: sum(
            self.dist_two_points(*segment)
            for segment in pairwise(path)
        ))[0] if paths else None

        return paths, shortest
