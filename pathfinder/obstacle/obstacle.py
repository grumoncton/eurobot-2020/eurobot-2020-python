from shapely.geometry import LineString


class Obstacle:
    @property
    def boundary(self):
        raise NotImplementedError()

    def is_blocking_segment(self, segment):
        line = LineString([(p.x, p.y) for p in segment])
        return not self.boundary.intersection(line).is_empty
