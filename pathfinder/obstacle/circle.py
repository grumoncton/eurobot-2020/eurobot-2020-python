from shapely.geometry import Point

from .obstacle import Obstacle


class Circle(Obstacle):
    def __init__(self, x, y, r):
        super().__init__()

        self.x = x
        self.y = y
        self.r = r

    def __str__(self):
        return 'Circle({}, {})'.format(self.x, self.y)

    def __repr__(self):
        return self.__str__()

    @property
    def center(self):
        return Point(self.x, self.y)

    @property
    def boundary(self):
        return self.center.buffer(self.r).boundary
