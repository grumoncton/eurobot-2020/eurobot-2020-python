import math
from os import path
import tkinter as tk

from lib import Coord, Arc, ResizingCanvas
from lib.functional import pairwise
from . import pathfinder
from .obstacle import Circle


class PathfinderCanvas(ResizingCanvas):
    # pylint: disable=too-many-ancestors

    def create_line(self, y1, x1, y2, x2, *args, **kwargs):
        # pylint: disable=arguments-differ
        return super().create_line(x1, y1, x2, y2, *args, **kwargs)

    def create_oval(self, y1, x1, y2, x2, *args, **kwargs):
        # pylint: disable=arguments-differ
        return super().create_oval(x1, y1, x2, y2, *args, **kwargs)

    def create_arc(self, y1, x1, y2, x2, *args, **kwargs):
        # pylint: disable=arguments-differ
        return super().create_arc(x1, y1, x2, y2, *args, **kwargs)


class Visualizer:
    def __init__(self):
        self.origin = None
        self.destination = None

        self.top = tk.Tk()

        frame = tk.Frame(self.top, bg='black')
        frame.pack(fill='both', expand=True)

        self.canvas = PathfinderCanvas(
            frame,
            height=pathfinder.TABLE_HEIGHT,
            width=pathfinder.TABLE_WIDTH,
            highlightthickness=0,
            bgimage=path.join(path.dirname(__file__), './vinyl.png'),
        )
        self.canvas.bind('<Button-1>', self.update_origin)
        self.canvas.bind('<Button-3>', self.update_destination)

        self.canvas.pack(fill='both', expand=True)

        for obstacle in pathfinder.obstacles:
            if isinstance(obstacle, Circle):
                self.canvas.create_circle(
                    obstacle.x,
                    obstacle.y,
                    obstacle.r,
                    fill='black',
                    stipple='gray50'
                )

            else:
                raise NotImplementedError()

    def mainloop(self):
        self.top.mainloop()

    def update_pathfinding(self):
        if not self.origin or not self.destination:
            return

        self.canvas.delete('path')

        paths, shortest = pathfinder.shortest_path(
            origin=self.origin,
            destination=self.destination,
        )

        # Create line between origin and destination
        # self.canvas.create_line(
        #     self.origin.x,
        #     self.origin.y,
        #     self.destination.x,
        #     self.destination.y,
        #     fill='blue',
        #     tags='path',
        # )

        for points in paths:
            self.draw_path(points)

        self.draw_path(shortest, colour='red')

    def update_origin(self, event):
        self.origin = Coord(
            x=event.y * pathfinder.TABLE_HEIGHT / self.canvas.height,
            y=event.x * pathfinder.TABLE_WIDTH / self.canvas.width,
        )
        print('Origin:', self.origin)

        self.canvas.delete('origin')
        self.canvas.create_circle(
            x=self.origin.x,
            y=self.origin.y,
            r=10,
            fill='blue',
            tags='origin',
        )

        self.update_pathfinding()

    def update_destination(self, event):
        self.destination = Coord(
            x=event.y * pathfinder.TABLE_HEIGHT / self.canvas.height,
            y=event.x * pathfinder.TABLE_WIDTH / self.canvas.width,
        )
        print('Destination:', self.destination)

        self.canvas.delete('destination')
        self.canvas.create_circle(
            x=self.destination.x,
            y=self.destination.y,
            r=10,
            fill='red',
            tags='destination',
        )

        self.update_pathfinding()

    def draw_path(self, points, colour='black'):
        for A, B in pairwise(points):
            p1 = A.p2 if isinstance(A, Arc) else A
            p2 = B.p1 if isinstance(B, Arc) else B

            if isinstance(A, Arc):

                a1 = math.degrees(math.atan2(
                    A.p1.x - A.center.x,
                    A.p1.y - A.center.y,
                ))
                a2 = math.degrees(math.atan2(
                    A.p2.x - A.center.x,
                    A.p2.y - A.center.y,
                ))

                r = pathfinder.dist_two_points(A.p1, A.center)

                extent = a1 - a2

                if extent > 180:
                    extent = extent - 360

                if extent < -180:
                    extent = 360 + extent

                self.canvas.create_arc(
                    A.center.x - r,
                    A.center.y - r,
                    A.center.x + r,
                    A.center.y + r,
                    start=-a1,
                    extent=extent,
                    width=3,
                    style='arc',
                    outline=colour,
                    tags='path',
                )

            # Show lines if arc not taken in dotted
                # self.canvas.create_line(
                #     p1.x, p1.y, A.intersection.x, A.intersection.y,
                #     fill=colour,
                #     dash=(4, 4),
                #     width=3,
                #     tags='path',
                # )

            # if isinstance(B, Arc):
                # self.canvas.create_line(
                #     p2.x, p2.y, B.intersection.x, B.intersection.y,
                #     fill=colour,
                #     dash=(4, 4),
                #     width=3,
                #     tags='path',
                # )

            self.canvas.create_line(
                p1.x, p1.y, p2.x, p2.y,
                fill=colour,
                width=3,
                tags='path',
            )
