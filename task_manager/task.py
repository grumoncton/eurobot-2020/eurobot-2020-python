from .run_steps_in_parallel import run_steps_in_parallel


class Task:
    def __init__(self, name, score, steps):
        import logging

        self.current_step = None
        self.name = name
        self.steps = steps
        self.score = score
        self.logger = logging.getLogger(__name__)

    def is_done(self):
        return not self.steps and self.current_step is None

    def get_new_steps(self):
        from . import BaseStep

        try:
            steps = self.steps.pop(0)

            if isinstance(steps, BaseStep):
                return (steps,)

            return steps
        except IndexError:
            self.current_step = None
            self.logger.debug('Out of steps in %s', self.name)
            return None

    def run(self):
        while True:
            steps = self.get_new_steps()

            if not steps:
                return

            run_steps_in_parallel(steps=steps)
