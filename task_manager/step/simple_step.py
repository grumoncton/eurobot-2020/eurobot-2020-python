from . import BaseStep


class SimpleStep(BaseStep):
    """
    Simple step with constant instruction and args.
    """

    device = None
    instruction = None
    args = None

    def __init__(self):
        self.name = 'SimpleStep: {}'.format(self.instruction)

        super().__init__()

    def run(self):
        from comms import send_instruction

        send_instruction(
            device=self.device,
            instruction=self.instruction,
            args=self.args,
        )
