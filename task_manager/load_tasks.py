from lib import dotenv


ROBOT_NAME = str(dotenv.get_value('ROBOT_NAME'))


def load_tasks():
    if ROBOT_NAME == 'primary':
        from task_manager.load_primary_tasks import load_primary_tasks

        return load_primary_tasks()

    if ROBOT_NAME == 'secondary':
        from task_manager.load_secondary_tasks import load_secondary_tasks

        return load_secondary_tasks()

    raise RuntimeError('No tasks found.')
