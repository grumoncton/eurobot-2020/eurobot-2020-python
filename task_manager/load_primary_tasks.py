import math
from lib import Coord, RobotSide, dotenv
from devices.mecanum import MoveToStep, MoveToRelativeStep, RotateToStep
from devices.primary_arms import LowerMainArmStep, RaiseMainArmStep, \
    LeadscrewExtendStep, LeadscrewRetractStep, SetSuckersStep, HomeInGameStep, \
    SemiMainArmStep, RaiseGoldArmStep, LowerGoldArmStep, \
    ActivateSolenoidsStep, TriggerExperienceStep
from devices.primary_sorter.steps import RaiseFlipperStep, LowerFlipperStep, \
    SetSortersStep, RaiseSlideBlockerStep, LowerSlideBlockerStep
from devices.primary_accelerator.steps import RaiseTilterStep, LowerTilterStep
from . import TaskManager, Task, TimeoutStep, SerialStep


IS_HOMO = bool(dotenv.get_value('IS_HOMO'))


def load_primary_tasks():
    if IS_HOMO:
        return TaskManager(tasks=[
            Task(name='Experiment', score=40, steps=[TriggerExperienceStep()]),
            Task(name='Homo', score=0, steps=[
                [
                    MoveToStep(Coord(x=1790, y=225)),
                    LowerMainArmStep(),
                ],
                SetSuckersStep(0xff),
                MoveToStep(Coord(x=450, y=225)),
                SetSuckersStep(0x00),
            ]),
        ])

    return TaskManager(tasks=[
        Task(name='Experiment', score=40, steps=[TriggerExperienceStep()]),

        Task(name='Scale', score=2 * 12, steps=[
            SetSortersStep(state=0x00),
            HomeInGameStep(),
            LowerSlideBlockerStep(RobotSide.RIGHT),
            [
                MoveToStep(Coord(x=1790, y=225)),
                LowerMainArmStep(),
            ],
            SetSuckersStep(0xff),
            [
                SerialStep(steps=[
                    MoveToStep(Coord(x=1310, y=225)),
                    MoveToStep(Coord(x=1310, y=615)),
                ]),
                SerialStep(steps=[
                    TimeoutStep(seconds=0.5),
                    LeadscrewExtendStep(),
                    RaiseMainArmStep(),
                    TimeoutStep(seconds=0.5),
                    SetSuckersStep(0x00),
                    LeadscrewRetractStep(),

                    RaiseFlipperStep(side=RobotSide.RIGHT),
                    TimeoutStep(seconds=0.5),
                    LowerFlipperStep(side=RobotSide.RIGHT),
                    TimeoutStep(seconds=0.5),
                    RaiseFlipperStep(side=RobotSide.RIGHT),
                    TimeoutStep(seconds=0.5),
                    LowerFlipperStep(side=RobotSide.RIGHT),

                    SetSortersStep(state=0b011),
                ]),
            ],
            LowerMainArmStep(),
            SetSortersStep(state=0b011000),
            LeadscrewExtendStep(),
            SetSuckersStep(0xff),

            TimeoutStep(seconds=0.2),
            HomeInGameStep(),
            SemiMainArmStep(),

            [
                SerialStep(steps=[
                    LeadscrewExtendStep(),
                    RaiseMainArmStep(),
                    TimeoutStep(seconds=2),
                    SetSuckersStep(0x00),
                    LeadscrewRetractStep(),
                    SetSortersStep(state=0b000111),
                ]),
                MoveToRelativeStep(Coord(x=-5, y=-100 * 3 * 1.0833)),
            ],

            LowerMainArmStep(),
            SetSortersStep(state=0b000000),
            LeadscrewExtendStep(),
            SetSuckersStep(0xff),

            TimeoutStep(seconds=0.2),
            HomeInGameStep(),
            SemiMainArmStep(),

            LeadscrewExtendStep(),
            RaiseMainArmStep(),
            TimeoutStep(seconds=1),
            SetSuckersStep(0x00),
            LeadscrewRetractStep(),

            RotateToStep(heading=math.radians(220)),

            MoveToRelativeStep(Coord(x=-320, y=-370)),

            RaiseSlideBlockerStep(RobotSide.RIGHT),
            RaiseSlideBlockerStep(RobotSide.LEFT),
            RaiseFlipperStep(side=RobotSide.RIGHT),
            TimeoutStep(seconds=0.2),
            LowerFlipperStep(side=RobotSide.RIGHT),
            TimeoutStep(seconds=1),
            RaiseFlipperStep(side=RobotSide.RIGHT),
            TimeoutStep(seconds=0.2),
            LowerFlipperStep(side=RobotSide.RIGHT),
        ]),

        Task(name='Accelerator', score=10 * 8, steps=[

            # Home
            MoveToRelativeStep(Coord(x=50, y=0)),
            MoveToRelativeStep(Coord(x=-150, y=-60)),

            MoveToRelativeStep(Coord(x=200, y=0)),
            MoveToStep(Coord(x=140, y=1550)),
            LowerGoldArmStep(side=RobotSide.LEFT),
            MoveToRelativeStep(Coord(x=0, y=-100)),
            [
                MoveToRelativeStep(Coord(x=0, y=100)),
                RaiseGoldArmStep(side=RobotSide.LEFT),
            ],

            RaiseTilterStep(),
            TimeoutStep(seconds=2),
            LowerTilterStep(),
            TimeoutStep(seconds=0.2),

            SetSortersStep(state=0b111000),
            TimeoutStep(seconds=0.5),

            RaiseTilterStep(),
            TimeoutStep(seconds=2),
            SetSortersStep(state=0b000111),
            LowerTilterStep(),
            TimeoutStep(seconds=0.2),
            SetSortersStep(state=0b111000),
            TimeoutStep(seconds=0.2),

            RaiseTilterStep(),
            TimeoutStep(seconds=2),
            LowerTilterStep(),
        ]),

        Task(name='Gold', score=24, steps=[
            MoveToRelativeStep(Coord(x=-100, y=-600)),
            LowerGoldArmStep(side=RobotSide.LEFT),
            ActivateSolenoidsStep(),
            SetSuckersStep(state=0b101),
            MoveToRelativeStep(Coord(x=40, y=0)),

            MoveToStep(Coord(x=1050, y=1100)),
            RotateToStep(heading=math.radians(200)),
            MoveToRelativeStep(Coord(x=250, y=0)),
        ]),
    ])
