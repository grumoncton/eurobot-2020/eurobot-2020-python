from lib.coord import Coord
from devices import secondary_mechanisms, wheels
from devices.primary_arms.steps import MoveGoldArmStep, SetSuckersStep
from . import TaskManager, Task


def load_test_tasks():
    return TaskManager(tasks=[
        Task(name='Test task', score=100, steps=[
            wheels.MoveToStep(destination=Coord(100, 100)),
            [
                secondary_mechanisms.LeadscrewStep(
                    state=secondary_mechanisms.LeadscrewState.CLEAR_HEIGHT,
                ),
                wheels.PathfindingMoveToStep(destination=Coord(150, 400))
            ],
            MoveGoldArmStep(
                side=MoveGoldArmStep.Side.RIGHT,
                state=MoveGoldArmStep.State.UP,
            ),
            SetSuckersStep(state=0b11111),
        ]),
    ])
